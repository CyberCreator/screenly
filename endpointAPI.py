
class CounterAsset(Resource):
    method_decorators = [api_response, auth_basic]

    @swagger.doc({
        'parameters': [
            {
                'name': 'Counter_assets',
                'in': 'formData',
                'type': 'string',
                'description':
                    '''
                    Content-Type: application/json
                    model: "{
                        "count_active_assets": 2,
                        "count_all_assets": 4,
                        "count_group_assets": "{
                            "imgGroup_assets": 1,
                            "webGroup_assets": 1,
                            "streamGroup_assets": 0,
                            "movGroup_assets": 2
                        }",
                        "count_name_assets": "{
                            "1": "name_assets",
                        }",
                        "count_time_assets": 860
                    }"
                    '''
            }
        ],
        'responses': {
            '200': {
                'description': 'Counter path',
                'schema': {
                    'type': 'string'
                }
            }
        }
    })
    # def group_assets(type, val):
    #    with db.conn(settings['database']) as conn:
    #        count_all_assets = len(assets_helper.read(conn))
    #        count_assets = 0
    #        assets = assets_helper.read(conn)
    #        for el in assets:
    #            if el[type] == val:
    #                count_assets += 1
    #                return count_assets
    def get(self):
        self.count_active_assets = 0
        self.web_group_assets = 0
        self.img_group_assets = 0
        self.mov_group_assets = 0
        self.stream_group_assets = 0
        self.count_name_assets = []
        self.i = 0
        self.time = 0
        with db.conn(settings['database']) as conn:
            assets = assets_helper.read(conn)
            count_all_assets = len(assets)
            for el in assets:
                if el["is_active"] == 1:
                    self.count_active_assets += 1
                if el["mimetype"] == "webpage":
                    self.web_group_assets += 1
                if el["mimetype"] == "image":
                    self.img_group_assets += 1
                if el["mimetype"] == "video":
                    self.mov_group_assets += 1
                if el["mimetype"] == "streaming":
                    self.stream_group_assets += 1
                if el['name'] :
                    self.i += 1
                    temp = [self.i, el['name']]
                    self.count_name_assets += [temp]
                if el["duration"]:
                    self.time += float(el["duration"])

        return {
            'count_all_assets': count_all_assets,
            'count_active_assets': self.count_active_assets,
            'count_group_assets': {
                'img_group_assets': self.img_group_assets,
                'web_group_assets': self.web_group_assets,
                'mov_group_assets': self.mov_group_assets,
                'stream_group_assets': self.stream_group_assets
            },
            'count_name_assets': self.count_name_assets,
            'count_time_assets': self.time
        }


api.add_resource(CounterAsset, '/api/v1/counter')
